window.addEventListener("load", function() {
	// Stran naložena
	
	//metoda ki se zgodi ob kliku "POTRDI"
	
	var Prijava = function() {
		var user = document.querySelector("#uporabnisko_ime").value;
		document.querySelector("#uporabnik").innerHTML = user;
		document.querySelector(".pokrivalo").style.visibility = "hidden";
	}
	
	document.querySelector("#prijavniGumb").addEventListener("click", Prijava);
	
	//metoda, ki se zgodi ob kliku "DODAJ OPOMNIK"
	
	var Dodaj = function() { 
		var naziv = document.querySelector("#naziv_opomnika").value;
		document.querySelector("#naziv_opomnika").value = "";
		var cas1 = document.querySelector("#cas_opomnika").value;
		document.querySelector("#cas_opomnika").value = "";
	
	  opomniki.innerHTML += " \
    <div class='senca rob opomnik'> \
        <div class='naziv_opomnika'>" + naziv + "</div> \
        <div class='cas_opomnika'> Opomnik čez <span>" + cas1 + 
            "</span> sekund.</div> \
    </div>";
};
	
	document.querySelector("#dodajGumb").addEventListener("click", Dodaj);
	
		
	// Posodobi opomnike
	var posodobiOpomnike = function() {
		var opomniki = document.querySelectorAll(".opomnik");
		
		for (var i = 0; i < opomniki.length; i++) {
			var opomnik = opomniki[i];
			var casovnik = opomnik.querySelector("span");
			var cas = parseInt(casovnik.innerHTML, 10);
	
			// TODO:
			if (cas == 0){ 
				var naziv_opomnika = opomnik.querySelector(".naziv_opomnika").innerHTML;
				alert("Opomnik!\n\nZadolžitev " + naziv_opomnika + " je potekla!");
				document.querySelector("#opomniki").removeChild(opomnik);
			}
			else { 
				casovnik.innerHTML = cas - 1;
			}
			// - sicer zmanjšaj čas za 1 in nastavi novo vrednost v časovniku
		}
	};
	
	setInterval(posodobiOpomnike, 1000);
	
});